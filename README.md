# Domain Whois
## Author: Eric Nicholls
### Licence: Lesser GPL 2.1

---

This is a simple bash script that will check if your desired domain is available or not. It comes with an extensive list of the most popular Top Level Domains (TLDs), Country Code TLDs, and General TLDs.

1. To run it, first you need to install **whois**.

**Debian/Ubuntu**
```
sudo apt -y install whois
```

**Fedora/Enterprise Linux**
```
sudo dnf -y install whois
```

**macOS**
```
brew install whois
```

---

2. Download the script, open the cloned directory, and make the script executable.
```
git clone https://gitlab.com/LatinCanuck/domain-whois.git && cd domain-whois && chmod +x domain-whois.sh
```

3. You can now run the script. No need to add the domain extension. Replace *mynewdomain123* with your desired domain.
```
./domain-whois.sh mynewdomain123
```

If you encounter any problem, Message me through Mastodon.
**@LatinCanuck@fosstodon.org**


