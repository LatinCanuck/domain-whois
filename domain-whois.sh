#!/bin/bash

# List of domain extensions to check
DOMAIN_EXTENSIONS=("com" "net" "org" "in" "us" "it" "info" "io" "co" "mx" "app" "dev" "company" "ca" "au" "art" "fr" "xyz" "de" "one" "life" "nl" "me" "care" "store" "how" "lol" "love" "online" "pl" "uk" "studio" "design" "nz" "day" "page" "run" "win" "jp" "email" "best" "blue" "place" "community" "eco" "space" "group" "agency" "rocks" "world" "works" "pro" "blog" "tech" "today" "site" "fun" "services" "earth" "plus" "video" "media" "chat" "network" "cc" "team" "global" "press" "city" "red" "green" "host" "uno" "one" "guru" "zone" "ninja")

# Function to check domain availability using whois
check_domain() {
  local domain=$1
  local extension=$2

  local whois_response
  whois_response=$(whois "${domain}.${extension}")

  if echo "$whois_response" | grep -qEi "(No match for|NOT FOUND|No Data Found|No entries found)"; then
    echo "${domain}.${extension} is available"
    return 0
  else
    return 1
  fi
}

# Main script
if [ "$#" -ne 1 ]; then
  echo "Usage: ./domain-whois.sh <domain_name>"
  exit 1
fi

domain_name=$1
available_domains=0

echo "Checking availability for ${domain_name}:"

for ext in "${DOMAIN_EXTENSIONS[@]}"; do
  if check_domain "$domain_name" "$ext"; then
    available_domains=$((available_domains + 1))
  fi
done

if [ $available_domains -eq 0 ]; then
  echo "No result found."
fi

